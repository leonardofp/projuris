package br.com.projuris.entity;

import java.math.BigDecimal;

public class Funcionario {
    
    public String departamento;
    public String Cargo;
    public BigDecimal salario;
    
    
    public Funcionario(String departamento, String cargo, BigDecimal salario) {
	super();
	this.departamento = departamento;
	Cargo = cargo;
	this.salario = salario;
    }
    
    
    public String getDepartamento() {
        return departamento;
    }
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
    public BigDecimal getSalario() {
        return salario;
    }
    public void setSalario(BigDecimal salario) {
        this.salario = salario;
    }
    public String getCargo() {
        return Cargo;
    }
    public void setCargo(String cargo) {
        Cargo = cargo;
    }
    
    
    

}
