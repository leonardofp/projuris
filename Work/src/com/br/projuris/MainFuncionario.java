package com.br.projuris;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.projuris.CustoCargo;
import br.com.projuris.CustoDepartamento;
import br.com.projuris.MyCalculo;
import br.com.projuris.entity.Funcionario;
public class MainFuncionario {
    
    public static void main(String[] args) {
	
//Funcionarios BEGIN
	
	Funcionario funcionario1 = new Funcionario("Assistente", "Administrativo", new BigDecimal(1000.0));
	Funcionario funcionario2 = new Funcionario("Gerente", "Administrativo", new BigDecimal(7000.70));
	Funcionario funcionario3 = new Funcionario("Diretor", "Administrativo", new BigDecimal(10000.45));
	Funcionario funcionario4 = new Funcionario("Assistente", "Financeiro", new BigDecimal(1300.9));
	Funcionario funcionario5 = new Funcionario("Gerente", "Financeiro", new BigDecimal(7500));
	Funcionario funcionario6 = new Funcionario("Diretor", "Financeiro", new BigDecimal(11000.0));
	Funcionario funcionario7 = new Funcionario("Estagiário", "Jurídico", new BigDecimal(700.4));
	Funcionario funcionario8 = new Funcionario("Assistente", "Jurídico", new BigDecimal(1800.90));
	Funcionario funcionario9 = new Funcionario("Gerente", "Jurídico", new BigDecimal(9500.50));
	Funcionario funcionario10 = new Funcionario("Diretor", "Jurídico", new BigDecimal(13000.0));
	
	List<Funcionario> funcionarios = new ArrayList<>();
	funcionarios.add(funcionario1);
	funcionarios.add(funcionario2);
	funcionarios.add(funcionario3);
	funcionarios.add(funcionario4);
	funcionarios.add(funcionario5);
	funcionarios.add(funcionario6);
	funcionarios.add(funcionario7);
	funcionarios.add(funcionario8);
	funcionarios.add(funcionario9);
	funcionarios.add(funcionario10);
	
	for(Funcionario f : funcionarios) {
	    System.out.println("Funcionários");
	    System.out.println(f.getDepartamento());
	    System.out.println(f.getSalario());
	    System.out.println(f.getCargo());
	}
//END	
	
//	MyCalculo
	MyCalculo calculo = new MyCalculo();
	
	List<CustoCargo> custoCargos = calculo.custoPorCargo(funcionarios);
	
	
	for(CustoCargo c : custoCargos) {
	    System.out.println("O custo por cargo é:");
	    System.out.println(c.getCargo());
	    System.out.println(c.getCusto());
	}
	
	List<CustoDepartamento> custoDepartamentos = calculo.custoPorDepartamento(funcionarios);
	
	for(CustoDepartamento c : custoDepartamentos) {
	    System.out.println("O custo por departamento é:");
	    System.out.println(c.getDepartamento());
	    System.out.println(c.getCusto());
	}
	
	
	
	
	
	
	
	
	
	
    }

}
